package com.company;
import java.util.Scanner;

public class Kalkulyator {

    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);
        System.out.print("Введите число: ");
        while (!scan.hasNextInt()) {
            System.out.println("Ошибка! Введите число: !");
            scan.next();
        }
        int x = scan.nextInt();

        System.out.print("Введите действие 1)+ 2)- 3)* 4)/ : ");
        while (!scan.hasNextInt()) {
            System.out.println("Ошибка! Введите действие 1)+ 2)- 3)* 4)/ : !");
            scan.next();
        }
        int z = scan.nextInt();

        System.out.print("Введите второе число: ");
        while (!scan.hasNextInt()) {
            System.out.println("Ошибка! Введите второе число: !");
            scan.next();
        }
        int y = scan.nextInt();
        if (z==1){
            int r = x + y;
            System.out.print(x + "+" + y + "=" + r);
        }
        else if (z==2){
            int r = x - y;
            System.out.print(x + "-" + y + "=" + r);
        }
        else if (z==3){
            int r = x * y;
            System.out.print(x + "*" + y + "=" + r);
        }
        else if (z==4){
            int r = x / y;
            System.out.print(x + "/" + y + "=" + r);
        }
    }

}
